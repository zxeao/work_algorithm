import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @ClassName FCFS
 * @Description TODO
 * @Author Zxeao
 * @Date 2022/3/24 下午4:29
 * @Version V1.0
 */
public class PSA {

    private int count;
    private Job[] jobs = null;

    private boolean input() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入需要调度的作业数：");

        this.count = scanner.nextInt();

        this.jobs = new Job[count];

        for (int i = 0; i < count; i++) {

            jobs[i] = new Job();

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的名称：");
            jobs[i].setName(scanner.next());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的到达时间：");
            jobs[i].setArrive_time(scanner.nextInt());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的运行时间：");
            jobs[i].setRun_time(scanner.nextInt());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的优先级：");
            jobs[i].setPriority(scanner.nextInt());

        }

        return true;
    }

    private void priority_sort(int i) {

        for ( ; i < count - 1; i++) {
            for (int j = count - 1; j > i ; j--) {
                if (jobs[j].getPriority() < jobs[j-1].getPriority()) {
                    Job job = jobs[j];
                    jobs[j] = jobs[j-1];
                    jobs[j-1] = job;
                }
            }
        }

    }

    private boolean psa() {

        for (int i = 0,j = 0; i < count; i++) {

            priority_sort(i);

            int k = 0;

            Boolean flag = false;

            if (jobs[i].getArrive_time() <= j) {
                k = i;
                flag = true;
            }else {
                for (k = i+1; k < count; k++) {
                    if (jobs[k].getArrive_time() <= j) {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag) {

                jobs[k].setStart_time(j);
                jobs[k].getRun_time();
                j += jobs[k].getRun_time();
                jobs[k].setEnd_time(j);
                jobs[k].computeSelf();

                Job job = jobs[k];
                jobs[k] = jobs[i];
                jobs[i] = job;

            }

        }

        return true;
    }

    private void output() {

        System.out.println("作业时间\t到达时间\t起始时间\t结束时间\t周转时间\t带权周转时间");

        for (int i = 0; i < count; i++) {
            System.out.println("   "+jobs[i].getName()+"   \t  "+jobs[i].getArrive_time()+"  \t  "+jobs[i].getStart_time()+"  \t  "+
                    jobs[i].getEnd_time() +"  \t  "+jobs[i].getTurnaround_time()+"  \t  "+jobs[i].getWeighted_turnaround_time());
        }

    }


    public static void main(String[] args) {

        PSA psa = new PSA();

        psa.input();

        psa.psa();

        psa.output();

    }

}

