import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @ClassName FCFS
 * @Description TODO
 * @Author Zxeao
 * @Date 2022/3/24 下午4:29
 * @Version V1.0
 */
public class RR {

    private int count;
    private Job[] jobs = null;

    private boolean input() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入需要调度的作业数：");

        this.count = scanner.nextInt();

        this.jobs = new Job[count];

        for (int i = 0; i < count; i++) {

            jobs[i] = new Job();

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的名称：");
            jobs[i].setName(scanner.next());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的到达时间：");
            jobs[i].setArrive_time(scanner.nextInt());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的运行时间：");
            jobs[i].setRun_time(scanner.nextInt());

        }

        return true;
    }

    private void bubble_sort() {

        for (int i = 0; i < count - 1; i++) {
            for (int j = 0; j < count - i -1; j++) {
                if (jobs[j].getArrive_time() > jobs[j+1].getArrive_time()) {
                    Job job = jobs[j];
                    jobs[j] = jobs[j+1];
                    jobs[j+1] = job;
                }
            }
        }

    }

    private boolean rr() {

        Queue<Job> queue = new LinkedList<Job>();

        bubble_sort();

        int time = 0, i = 0;

        for (int j = 0; j < count; j++) {
            queue.offer(jobs[j]);
        }

        while (time != -1) {
            Job job = queue.poll();

            if (job.getArrive_time() <= time) {
                if (job.getRan_time() == 0) {
                    job.setStart_time(time);
                    job.setRan_time(job.getRan_time() + 1);
                } else if (job.getRan_time() == job.getRun_time()) {
                    System.out.println("i->"+i);
                    jobs[i] = job;
                    jobs[i].setEnd_time(time);
                    jobs[i].computeSelf();

                    if (++i>=count) {
                        break;
                    }
                } else {
                    queue.offer(job);
                }

            }

            time++;

        }

        return true;
    }

    private void output() {

        System.out.println("作业时间\t到达时间\t起始时间\t结束时间\t周转时间\t带权周转时间");

        for (int i = 0; i < count; i++) {
            System.out.println("   "+jobs[i].getName()+"   \t  "+jobs[i].getArrive_time()+"  \t  "+jobs[i].getStart_time()+"  \t  "+
                    jobs[i].getEnd_time() +"  \t  "+jobs[i].getTurnaround_time()+"  \t  "+jobs[i].getWeighted_turnaround_time());
        }

    }


    public static void main(String[] args) {

        RR rr = new RR();

        rr.input();

        rr.rr();

        rr.output();

    }

}
