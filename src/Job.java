/**
 * @ClassName Job
 * @Description TODO
 * @Author Zxeao
 * @Date 2022/3/24 下午4:38
 * @Version V1.0
 */
public class Job {

    private String name;
    private int arrive_time;
    private int run_time;

    private int ran_time;

    private int start_time;
    private int end_time;
    private int turnaround_time;
    private double weighted_turnaround_time;

    private int priority;

    private double response_priority;

    public Job() {
        ran_time = 0;
    }

    public void computeSelf() {
        this.turnaround_time = this.end_time - this.arrive_time;
        this.weighted_turnaround_time = this.turnaround_time*1.0 / this.run_time;
    }

    public int getRan_time() {
        return ran_time;
    }

    public void setRan_time(int ran_time) {
        this.ran_time = ran_time;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public double getResponse_priority() {
        return response_priority;
    }

    public void computeResponse_priority(int time) {
        this.response_priority = (time - arrive_time + run_time)*1.0/run_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArrive_time() {
        return arrive_time;
    }

    public void setArrive_time(int arrive_time) {
        this.arrive_time = arrive_time;
    }

    public int getRun_time() {
        return run_time;
    }

    public void setRun_time(int run_time) {
        this.run_time = run_time;
    }

    public int getStart_time() {
        return start_time;
    }

    public void setStart_time(int start_time) {
        this.start_time = start_time;
    }

    public int getEnd_time() {
        return end_time;
    }

    public void setEnd_time(int end_time) {
        this.end_time = end_time;
    }

    public int getTurnaround_time() {
        return turnaround_time;
    }

    public double getWeighted_turnaround_time() {
        return weighted_turnaround_time;
    }

}
