import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * @ClassName FCFS
 * @Description TODO
 * @Author Zxeao
 * @Date 2022/3/24 下午4:29
 * @Version V1.0
 */
public class FCFS {

    private int count;
    private Job[] jobs = null;

    private boolean input() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入需要调度的作业数：");

        this.count = scanner.nextInt();

        this.jobs = new Job[count];

        for (int i = 0; i < count; i++) {

            jobs[i] = new Job();

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的名称：");
            jobs[i].setName(scanner.next());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的到达时间：");
            jobs[i].setArrive_time(scanner.nextInt());

            System.out.print("请输入作业"+ String.valueOf(i + 1) +"的运行时间：");
            jobs[i].setRun_time(scanner.nextInt());

        }

        return true;
    }

    private void bubble_sort() {

        for (int i = 0; i < count - 1; i++) {
            for (int j = 0; j < count - i -1; j++) {
                if (jobs[j].getArrive_time() > jobs[j+1].getArrive_time()) {
                    Job job = jobs[j];
                    jobs[j] = jobs[j+1];
                    jobs[j+1] = job;
                }
            }
        }

    }

    private boolean fcfs() {

        int all_run_time = 0;

        Queue<Job> queue = new LinkedList<Job>();

        for (int i = 0; i < count; i++) {
            all_run_time += jobs[i].getRun_time();
        }

        bubble_sort();

        for (int i = 0; i < count; i++) {
            queue.offer(jobs[i]);
        }

        for (int i = 0,j = 0; i < all_run_time; j++) {
            if (queue.element().getArrive_time() <= j) {
                queue.element().setStart_time(j);
                j += queue.element().getRun_time();
                i += queue.element().getRun_time();
                queue.element().setEnd_time(j);
                queue.element().computeSelf();
                queue.remove();
                j--;
            }
        }

        return true;
    }

    private void output() {

        System.out.println("作业时间\t到达时间\t起始时间\t结束时间\t周转时间\t带权周转时间");

        for (int i = 0; i < count; i++) {
            System.out.println("   "+jobs[i].getName()+"   \t  "+jobs[i].getArrive_time()+"  \t  "+jobs[i].getStart_time()+"  \t  "+
                    jobs[i].getEnd_time() +"  \t  "+jobs[i].getTurnaround_time()+"  \t  "+jobs[i].getWeighted_turnaround_time());
        }

    }


    public static void main(String[] args) {

        FCFS fcfs = new FCFS();

        fcfs.input();

        fcfs.fcfs();

        fcfs.output();

    }

}
